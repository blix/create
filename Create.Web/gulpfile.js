/// <binding ProjectOpened='watch' />
var gulp = require('gulp'),
    browserify = require('browserify'),
    sass = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    minifycss = require('gulp-minify-css'),
    source = require('vinyl-source-stream'),
    rename = require('gulp-rename'),
    buffer = require('vinyl-buffer');

gulp.task('sass', function () {
    return gulp.src('local/sass/base.scss')
        .pipe(sass())
        .pipe(autoprefixer({ browsers: ['last 5 versions'] }))
        .pipe(minifycss())
        .pipe(gulp.dest('public/css'));
});

gulp.task('js', function () {
    // the reason I'm creating separately is that babelify doesn't like FSS.js
    browserify(['local/js/app'])
        .transform('babelify', { presets: ['es2015'] })
        .bundle()
        .pipe(source('app.js'))
        .pipe(buffer())
        .pipe(gulp.dest('./public/js'));

    return browserify(['local/js/site.js'])
        .transform('babelify', { presets: ['es2015'] })
        .bundle()
        .pipe(source('site.js'))
        .pipe(buffer())
        .pipe(rename({ suffix: '.min' }))
        .pipe(gulp.dest('./public/js'));
});


gulp.task('watch', function () {
    gulp.watch('local/**/*.scss', ['sass']);
    gulp.watch('local/**/*.js', ['js']);
});