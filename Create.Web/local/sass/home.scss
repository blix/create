﻿@import "vars.scss";

.about {
    margin: 50px 0;

    @media(min-width: $screen-md) {
        margin: 80px 0;
    }

    &-content {



        h4 {
            color: $brand-purple;
        }

        &-main {
            @media(min-width: $screen-md) {
                width: 75%;
            }

            h1 {
                font-weight: 300;
                margin: 0px 0 25px 0;
            }

            h2 {
                font-weight: 300;
                margin: 0px 0 25px 0;
            }

            p {
                margin-bottom: 20px;

                &:last-child {
                    margin-bottom: 0;
                }
            }
        }

        &-list {
            margin-top: 50px;


            &-flex {
                display: flex;
                justify-content: space-between;
                align-items: flex-end;
                flex-direction: column;

                @media(min-width: $screen-md) {
                    flex-direction: row;
                }

                ul {
                    margin-top: 20px;
                    list-style: none;
                    width: 100%;

                    @media(min-width: $screen-md) {
                        width: 45%;
                    }

                    li {
                        font-weight: 700;
                        margin-bottom: 10px;

                        &:last-child {
                            margin-bottom: 0;
                        }
                    }
                }

                .logos {
                    width: 100%;
                    display: flex;
                    justify-content: space-between;
                    margin-top: 30px;
                    padding: 10px;

                    @media(min-width: $screen-md) {
                        width: 50%;
                        margin-top: 0;
                    }
                }
            }
        }
    }
}


.vr {
    background-position: center center;
    background-size: cover;
    background-repeat: no-repeat;
    width: 100%;
    position: relative;

    .far {
        color: $brand-white;
        font-size: 50px;
    }

    &.off {
        background-image: none;

        &:after {
            content: none;
        }

        .vr-content {
            display: none;
        }

        video {
            z-index: 1 !important;
            position: relative !important;
            display: block;
        }
    }

    video {
        width: 100%;
        height: 100%;
        position: absolute;
        top: 0;
        left: 0;
        right: 0;
        z-index: -1;
    }

    h4 {
        color: $brand-purple;
        margin-bottom: 20px;

        a {
           
        }
    }

    @media(min-width: $screen-md) {
    }

    &:after {
        position: absolute;
        width: 100%;
        bottom: 0;
        top: 0;
        left: 0;
        right: 0;
        content: "";
        background: linear-gradient( -91deg, rgba(13,0,45, .4) 0%, rgba(135,28,102, .4) 100%);
    }

    &-content {
        position: relative;
        z-index: 3;
        text-align: center;
        padding: 100px 0;

        @media(min-width: $screen-md) {
            padding: 15vh 0;
        }

        h1 {
            color: $brand-white;
            font-weight: 700;
            margin: 60px auto 60px auto;
            width: 90%;

            @media(min-width: $screen-sm) {
                width: 70%;
            }
        }

        h4 {
            color: $brand-white;
            font-weight: 300;
       

            a {
              
            }
        }

        .showvid {
            cursor: pointer;
        }
    }
}


.vr-no {
    background-position: center center;
    background-size: cover;
    background-repeat: no-repeat;
    width: 100%;
    position: relative;

    video {
        width: 100%;
        height: 100%;
    }

    h4 {
        margin-bottom: 20px;
        text-decoration: underline !important;
        text-decoration-color: $brand-purple;

        a {
        }
    }
}


.sliderContainer {
    position: relative;
    height: 50vh;

    @media(min-width: $screen-md) {
        height: 80vh;
    }
}

.slider {
    width: 100%;
    height: 50vh;

    @media(min-width: $screen-md) {
        height: 80vh;
    }

    &-text {
        /*
        padding: 300px 0 90px 0;
        z-index: 10;
        position: relative;

        @media(min-width: $screen-lg) {
            width: 80%;
            padding: 550px 0 90px 0;
        }

        h2 {
            font-weight: 700;
            opacity: 0;
        }
            */
        display: none;
    }

    div {
        @include background-cover();
        height: 50vh;

        @media(min-width: $screen-md) {
            height: 80vh;
        }
    }
}

.slick-slide {
    color: white;
    position: relative;

    &:before {
        content: "";
        bottom: 0;
        left: 0;
        top: 0;
        right: 0;
        height: 100%;
        width: 100%;
        background-color: rgba(0,0,0, .2);
        position: absolute;
        z-index: 1;
    }
}

.progressBarContainer {
    position: absolute;
    bottom: 60px;
    width: 100%;
}

.progressBarSize {
    display: block;
    width: 2%;
    padding: 0;
    cursor: pointer;
    margin-right: 2%;
    float: left;
    color: white;
    min-width: 26px;
    margin-bottom: 10px;
}



.progressBarContainer div span.progressBar {
    width: 100%;
    height: 2px;
    background-color: rgba(255, 255, 255, 0.6);
    display: block;
}

.progressBarContainer div span.progressBar .inProgress {
    background-color: $brand-purple;
    width: 0%;
    height: 2px;
}


.gda {
    max-width: 250px;
    margin-top: 30px;
    display: block;
    margin: 30px auto;
    @media(min-width: $screen-sm) {
        position: absolute;
        top: 50%;
        right: 0;
        margin: 0;
    }

    @media(min-width: $screen-md) {
        top: 0;
        right: -50px;
    }

    &--2 {

        @media (min-width: $screen-sm) {
            top: 70%;
        }

        @media (min-width: $screen-md) {
            top: 100px;
        }
    }
}