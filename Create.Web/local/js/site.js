﻿$(document).ready(function () {
    var $hamburger = $(".hamburger");
    $hamburger.on("click", function (e) {
        $('.mobnav-links').toggleClass('on');
        $hamburger.toggleClass("is-active");
        $('body').css('hidden');
    });


    $(".slider").slick({
        infinite: true,
        arrows: false,
        dots: false,
        autoplay: false,
        speed: 1500,

        slidesToScroll: 1
    });

    //ticking machine
    var percentTime;
    var tick;
    var time = 1;
    var progressBarIndex = 0;
    var fullWidth = 30;

    $('.progressBarContainer .progressBar').each(function (index) {
        var progress = "<div class='inProgress inProgress" + index + "'></div>";
        $(this).html(progress);
    });

    function startProgressbar() {
        resetProgressbar();
        var i = 0;
        percentTime = 0;
        tick = setInterval(interval, 12);
        for (i = 0; i > 12; i++) {
            $('.pbs-' + i).css({
                width: "2%"
            });
        }

    }

    function interval() {
        if (($('.slider .slick-track div[data-slick-index="' + progressBarIndex + '"]').attr("aria-hidden")) === "true") {
            progressBarIndex = $('.slider .slick-track div[aria-hidden="false"]').data("slickIndex");
            startProgressbar();



        } else {
            percentTime += 1 / (time + 5);
            $('.inProgress' + progressBarIndex).css({
                width: percentTime + "%"
            });

            $('.pbs').css({
                width: "2%"
            });
            $('.pbs-' + progressBarIndex).css({
                width: "50%"
            });
            


            if (percentTime >= 100) {
                $('.pbs-' + progressBarIndex).css({
                    width: "2%"
                });
                $('.single-item').slick('slickNext');
                progressBarIndex++;
                if (progressBarIndex > 10) {
                    progressBarIndex = 0;
                }
                startProgressbar();

            }
        }
    }

    function resetProgressbar() {
        $('.inProgress').css({
            width: 0 + '%'
        });

        clearInterval(tick);
    }
    startProgressbar();
   

    $('.progressBarContainer div').click(function () {
        clearInterval(tick);
        var goToThisIndex = $(this).find("span").data("slickIndex");
        $('.single-item').slick('slickGoTo', goToThisIndex, false);
        startProgressbar();
        $('.pbs' + progressBarIndex).css({
            width: "2%"
        });
    });
});

$(".people-flex-container").click(function () {
    var popupclass = ".employee-pop-up-" + $(this).data("id");
    $(".employee-overlay").addClass("on");
    $(popupclass).addClass("on");
});

$(".employee-overlay, .employeepopup i").click(function (e) {
    $(".employee-overlay").removeClass("on");
    $(".employeepopup").removeClass("on");
});


$(".view-gallery").click(function (e) {
    $(".slide").addClass("on");
    $(".slide-overlay").addClass("on");
    $("body").css("overflow", "hidden");
});


$(".view-gallery-thumb").click(function (e) {
    $(".slide").addClass("on");
    $(".slide-overlay").addClass("on");
    $("body").css("overflow", "hidden");
    console.log($(this).data("id"));
});



$(".showvid").click(function (e) {
    $(".vr").addClass("off");
});



$(".gallery-close").click(function (e) {
    $(".slide").removeClass("on");
    $(".slide-overlay").removeClass("on");
    $("body").css("overflow", "scroll");
});



function checkTransit() {
    var viewTop = $(window).scrollTop();
    var viewBottom = viewTop + window.innerHeight;
    $('.transit').each(function () {
        var top = $(this).offset().top;
        var bottom = top + this.clientHeight;
        var topOnscreen = (top > viewTop) && (top < viewBottom);
        var botOnscreen = (bottom > viewTop) && (bottom < viewBottom);
        var middleOnScreen = (top <= viewTop) && (bottom >= viewBottom);
        if (topOnscreen || middleOnScreen || botOnscreen) {
            $(this).addClass('transitted');
        }
    });
}
checkTransit()

$(window).on('scroll resize', function () {
    checkTransit()
})


$(".showvid").click(function () {
    $('#video').get(0).play();
});

$("#newsclick").click(function () {
    $('#newsmore').addClass('on');
    $(this).addClass('off');
});

